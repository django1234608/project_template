# CHECK IF .ENV FILE EXISTS (YOU SHOULD HAVE ONE)

ifneq (,$(wildcard ./.env))
    # import .env file intro makefile
    include .env
    # export all variables
    export
# Then you have make variables for all your env, for example MY_VAR use as $(MY_VAR)
endif



# DETERMINE THE CONTAINER RUNTIME

CONTAINER_RUNTIME := $(shell command -v podman 2>/dev/null || command -v docker 2>/dev/null)
COMPOSE_RUNTIME := $(shell command -v podman-compose 2>/dev/null || command -v docker-compose 2>/dev/null)



# CHECK IF A CONTAINER RUNTIME WAS FOUND

ifeq ($(strip $(CONTAINER_RUNTIME)),)
    $(error Neither podman nor docker found in $(PATH))
endif
ifeq ($(strip $(COMPOSE_RUNTIME)),)
    $(error Neither podman nor docker found in $(PATH))
endif



# Default target is to print help
default: help
.PHONY: help
help: ## display this help message
	@echo "Please use \`make <target>' where <target> is one of"
	@awk 'BEGIN {FS = ":[^:=]*?## "}; /^[^#[:space:]].*?:[^:=]*?## / {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)



# CONTAINERS RELATED TARGETS

just_login: ## Login to the Docker hub
	$(CONTAINER_RUNTIME) login -u ${DOCKERHUB_USERNAME} -p ${DOCKERHUB_PASSWORD} docker.io

just_build_image: just_login ## Build the image
	$(CONTAINER_RUNTIME) build --squash-all --no-cache \
	--tag ${DOCKERHUB_USERNAME}/${DOCKERHUB_APP_NAME}:${DOCKERHUB_APP_VERSION} \
	-f ./containerfiles/Containerfile.api .

just_push_image: just_build_image ## Push the image to the Docker hub
	$(CONTAINER_RUNTIME) push ${DOCKERHUB_APP_NAME}:${DOCKERHUB_APP_VERSION} \
	docker.io/${DOCKERHUB_USERNAME}/${DOCKERHUB_APP_NAME}:${DOCKERHUB_APP_VERSION}

just_run_it: just_build_image ## Start a container based on the image
	$(CONTAINER_RUNTIME) run \
	-e DJANGO_SETTINGS_MODULE=project.settings.dev \
	-e DEBUG=True \
	-e PYTHONUNBUFFERED=1 \
	-p 8000:8000 \
	${DOCKERHUB_USERNAME}/${DOCKERHUB_APP_NAME}:${DOCKERHUB_APP_VERSION}

just_compose: ## Start orchestration using Compose file
	$(COMPOSE_RUNTIME) -f ./compose.yaml up --build --force-recreate --abort-on-container-exit

just_clean_containers: # Stop then remove all containers + volumes
	$(COMPOSE_RUNTIME) down
	$(CONTAINER_RUNTIME) stop -a
	$(CONTAINER_RUNTIME) rm -a -f

just_clean_images: # Remove all images
	$(CONTAINER_RUNTIME) rmi -a

just_clean_volumes: # Remove all volumes
	$(CONTAINER_RUNTIME) volume rm --all

just_clean: just_clean_containers just_clean_volumes just_clean_images



# DJANGO DEV RELATED TARGETS

dj_migrate: ## Migrate Django database
	DJANGO_SETTINGS_MODULE=project.settings.dev \
	python3 manage.py makemigrations && \
	DJANGO_SETTINGS_MODULE=project.settings.dev \
	python3 manage.py migrate

dj_reset_db: ## Reset Django database
	DJANGO_SETTINGS_MODULE=project.settings.dev \
	python3 manage.py reset_db

dj_test: ## Launch unit tests
	DJANGO_SETTINGS_MODULE=project.settings.dev \
	pytest

dj_run: ## Run Django project
	DJANGO_SETTINGS_MODULE=project.settings.dev \
	python3 manage.py runserver 0.0.0.0:8000

dj_virtualenv: ## Create virtual env
	python3 -m venv .venv --upgrade-deps
	.venv/bin/python -m pip install -r requirements/test.in

dj_makemessages: ## Generate translation messages
	DJANGO_SETTINGS_MODULE=project.settings.dev \
	django-admin makemessages --all --ignore=env


dj_compilemessages: ## Compile translation messages
	DJANGO_SETTINGS_MODULE=project.settings.dev \
	django-admin compilemessages --ignore=env


# PROJECT CREATION RELATED TARGETS

start_project:
	django-admin startproject $(PROJECT_NAME) \
	--template \
	$(PROJECT_TPL)

start_core_app:
	mkdir applications/core
	django-admin startapp \
	--template $(CORE_APP_TPL) \
	core applications/core

start_app:
	mkdir applications/$(APP_NAME)
	django-admin startapp \
	--template $(APP_TPL) \
	$(APP_NAME) applications/$(APP_NAME)
