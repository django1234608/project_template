"""
Django test settings for patient_microservice project.

"""

from dotenv import load_dotenv

from .base import *  # noqa: F401, F403

load_dotenv()

INSTALLED_APPS += [  # noqa: F405
    "django_extensions",
]
