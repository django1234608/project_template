import json

import pytest
from django.utils import timezone
from django.utils.http import urlencode
from rest_framework.reverse import reverse
from rest_framework.test import APIRequestFactory, force_authenticate

from applications.core.api.v1 import views

from fixtures import * # noqa F401 F403


@pytest.mark.django_db
def test_data_model(a_user):
    """
    Test if the data model is returned as a CSV file
    """
    factory = APIRequestFactory()
    view = views.data_dict
    baseurl = f'{reverse("applications.core:database-data-dictionary")}'
    request = factory.get(baseurl)
    force_authenticate(request, a_user)
    response = view(request)
    assert response.status_code == 200
    assert response.headers['Content-Type'] == 'text/csv', "Response is not a CSV file"
    assert int(response.headers['Content-Length']) > 1
    assert response.filename.startswith('data-dictionary-')
    assert response.filename.endswith('.csv')


@pytest.mark.django_db
def test_status(a_user):
    """
    Test if the status is returned
    """
    factory = APIRequestFactory()
    view = views.status
    baseurl = f'{reverse("applications.core:health-check")}'
    request = factory.get(baseurl)
    force_authenticate(request, a_user)
    response = view(request)
    assert response.status_code == 200
    assert json.loads(response.content.decode()) == {'message': 'OK'}
