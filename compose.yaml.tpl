name: "{{project_name}}"
volumes:
  pg_data_volume:
  django_logs_volume:
  media_volume:
  static_volume:

networks:
  backend:
    driver: bridge

services:
  database:
    image: postgres:16.4
    container_name: database
    hostname: database
    restart: always
    environment:
      POSTGRES_DB: postgres
      POSTGRES_PORT: 5432
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: postgres
      POSTGRES_HOST_AUTH_METHOD: trust
    ports:
      - 15432:5432
    volumes:
      - ./configurationfiles/db:/docker-entrypoint-initdb.d
      - pg_data_volume:/var/lib/postgresql/data
    networks:
      - backend
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U postgres"]
      retries: 5
      interval: 3s
      timeout: 40s

  api:
    container_name: api
    hostname: api
    restart: always
    environment:
      DJANGO_POSTGRES_HOST: 'database'
      DJANGO_POSTGRES_DB: 'postgres'
      DJANGO_POSTGRES_PORT: 5432
      DJANGO_POSTGRES_USER: 'postgres'
      DJANGO_POSTGRES_PASSWORD: 'postgres'
      SECRET_KEY: '^v1mb0b7fkx(m5s!x6sovu12op3q_b88+$&et$t2@w#0!!!ovb'
      DEBUG: False
      ALLOWED_HOSTS: "*"
      DJANGO_SUPERUSER_USERNAME: nsukami
      DJANGO_SUPERUSER_EMAIL: ptrck@nskm.xyz
      DJANGO_SUPERUSER_PASSWORD: dKsst14/040608
      DJANGO_SETTINGS_MODULE: project.settings.production
    build:
      context: .
      dockerfile: ./containerfiles/Containerfile.api
    volumes:
      - django_logs_volume:/usr/src/app/logs
      - media_volume:/media
      - static_volume:/static
    networks:
      - backend
    depends_on:
      - database
    links:
      - database
    healthcheck:
      test: curl --fail http://localhost:8000/ || exit 1
      interval: 5s
      timeout: 40s
      retries: 5

  webserver:
    container_name: webserver
    hostname: webserver
    image: nginx:1.27.1
    restart: unless-stopped
    ports:
      # The HTTP port
      - "1080:80"
    volumes:
      - ./configurationfiles/nginx/conf.d:/etc/nginx/conf.d
      - media_volume:/media
      - static_volume:/static
    depends_on:
      - api
    networks:
      - backend
