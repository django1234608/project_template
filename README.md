# Introduction

## Purpose

TODO


## Intended Audience:

The intended and primary audience of this document are all the people at IPD labs dealing with PCR plate plans, all the people at the DDSI. This document will be written with no technical terms (_hopefully_) for the benefit of everyone on the team. It will define the business rules of this application.

## Scope:

TODO

# Overall Description of the product:

TODO

## Installation and Usage:

To run the project locally, one may want to :

- git clone the repository: `git clone ...`
- cd into the repository `cd ...`
- copy the file `env.sample` to `.env`
- edit the file `.env` and update all the environment variables according to your needs
- create a Python virtualenv `python -m venv .env`
- activate the Python virtualenv `source .env/bin/activate`
- install Dependencies: `pip install -r requirements/dev.in`
- make sure to have PostgreSQL installed: `sudo apt install postgresql`
- install pre-commit hooks: `pre-commit install`
- type the following commands to run the project:
  - `python manage.py migrate && python manage.py createsuperuser --no-input`
  - `python manage.py runserver`
- open a web browser and visit the following url: `http://localhost:8000/api/v1/schema/swagger-ui/`

Alternatively:

- git clone the repository: `git clone ...`
- cd into the repository `cd ...`
- copy the file `env.sample` to `.env`
- edit the file `.env` and update all the environment variables according to your needs
- copy the file `containerfiles/Containerfile.api` to `containerfiles/Containerfile.dev.api` (1)
- copy the file `compose.yaml` to `compose.dev.yaml` (2)
- edit (1) & (2) as you need
- run the project using docker-compose
- open a web browser and visit the following url: `http://localhost:8000/api/v1/schema/swagger-ui/`

## Built With:

This product was exclusively built using [free software]()

- [Debian](https://debian.org/)
- [Python](https://python.org/)
- [Django](https://djangoproject.com)
- [Postgresql](https://postgresql.org)
- [Nginx](https://nginx.org)


All the other dependencies can be found inside the `requirements` folder.


## Project structure:

```text
.
├── applications
│   ├── core                                 # Core: any code that can be reused in other projects
│   │   ├── management
│   │   │   ├── commands
│   │   │   │   ├── createsuperusers.py      # Custom Django cmd to create 4 default super users
│   │   │   │   ├── generator.py             # Custom Django cmd to generate code
│   │   │   │   └── insert.py                # Custom Django cmd to insert data inside tables
│   │   │   └── __init__.py
│   │   └── urls.py                          # one url to serve the data dictionary
│   └── your_application
│       ├── admin.py
│       ├── api
│       │   ├── urls.py
│       │   └── v1
│       │       ├── filters.py               # global search and filtering
│       │       ├── pagination.py            # pagination
│       │       ├── serializers.py           # serialization
│       │       ├── tests
│       │       │   ├── data                 # data used during testing
│       │       │   ├── fixtures.py
│       │       │   ├── tests_commands.py
│       │       │   ├── tests_models.py
│       │       │   └── tests_views.py
│       │       ├── urls.py
│       │       └── views.py
│       ├── apps.py
│       ├── constants.py                     # Any useful constant definition
│       ├── data                             # data used to populate database
│       ├── events.py
│       ├── exceptions.py
│       ├── managers.py
│       ├── migrations                       # database migrations scripts
│       ├── models.py                        # entities definition
│       ├── pagination.py                    pagination configuration
│       ├── querysets.py
│       ├── signals
│       ├── tasks.py
│       ├── tests
│       │   ├── fixtures.py
│       │   └── __init__.py
│       └── urls.py
├── bandit.yaml
├── bitbucket-pipelines.yml                  # CI/CD configuration 
├── commons
│   ├── __init__.py
│   └── utils.py
├── compose.yaml
├── compose.dev.yaml                         # Create if you want your own docker-compose file
├── configurationfiles
│   ├── db
│   └── nginx
├── containerfiles
│   ├── Containerfile.api                    # Containerfile used for deployment
│   └── Containerfile.api.dev                # Create if you want your own Dockerfile
├── data                                     # Folder containing fixtures data
├── docs
├── env.sample                               # Environment variables
├── LICENSE
├── locale                                   # Translation folders
│   ├── en
│   ├── es
│   └── fr
├── logs
├── Makefile                                 # Most useful commands for any developer involved
├── manage.py                                # Django entry point
├── mkdocs.yaml                              # Documentation configuration
├── .pre-commit-config.yaml                  # Configuration file for pre-commit package
├── project                                  # Configuration folder
│   ├── asgi.py
│   ├── celery.py
│   ├── __init__.py
│   ├── settings                             # Django settings 
│   ├── urls
│   │   ├── __init__.py
│   │   └── v1
│   │       ├── __init__.py
│   │       └── urls.py
│   └── wsgi.py
├── pytest.ini
├── requirements
└── tox.ini
```

## Related git repositories

- [Class diagrams](https://bitbucket.org/ipd-ddsi/ipd_data_modeling_and_designs/src/trunk/)

#### 4S

- [Patient Service](https://bitbucket.org/ipd-ddsi/patient_microservice/src/trunk/)
- [Consultation Service](https://bitbucket.org/ipd-ddsi/consultation_microservice/src/main/)
- [Reference Service](https://bitbucket.org/ipd-ddsi/reference_microservice/src/main/)

#### SuperApp

- [Exam Service](https://bitbucket.org/ipd-ddsi/superapp-exam-service/src/main/)
- [Identity Service](https://bitbucket.org/ipd-ddsi/superapp-identity-service/src/main/)
- [Organization Service](https://bitbucket.org/ipd-ddsi/superapp-organization-service/src/main/)
- [Appointment Service](https://bitbucket.org/ipd-ddsi/superapp-appointment-service/src/develop/)


## Contact

DIF: equipe-dif@pasteur.sn
